# Ubuntu Laptop Provision

This ansible playbook provisions my ThinkPad L580 running a fresh install of Ubuntu 19.04.

## Run playbook

```bash
ansible-playbook main.yml --ask-vault-pass -i hosts
```

## One-line provision

```bash
wget https://tinyurl.com/laptop-provision && sh laptop-provision
```